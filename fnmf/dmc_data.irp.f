 BEGIN_PROVIDER [ double precision, h_dmc_row , (N_det) ]
&BEGIN_PROVIDER [ double precision, s_dmc_row , (N_det) ]
 implicit none
 BEGIN_DOC
 ! Data sampled with QMC=Chem
 END_DOC

 h_dmc_row(:) = h_dmc(:)
 s_dmc_row(:) = s_dmc(:)
 call dset_order(h_dmc_row,psi_bilinear_matrix_order_reverse,N_det)
 call dset_order(s_dmc_row,psi_bilinear_matrix_order_reverse,N_det)

!  integer :: i
!  do i=1,N_det
!    s_dmc_row(i) = psi_coef(i,1)
!    call i_h_psi(psi_det(1,1,i), psi_det, psi_coef, N_int, N_det, &
!         N_det, 1, h_dmc_row(i) )
!  enddo

END_PROVIDER

BEGIN_PROVIDER [ integer, mat_size ]
 implicit none
 BEGIN_DOC
! Size of the matrices
 END_DOC
 mat_size = N_det+1
END_PROVIDER

BEGIN_PROVIDER [ double precision, H_dmc_mat, (mat_size, mat_size) ]
 implicit none
 BEGIN_DOC
! Hamiltonian extended with DMC data
 END_DOC
 integer :: i,j
 do j=1,N_det
   do i=1,N_det
      call i_h_j(psi_det(1,1,i), psi_det(1,1,j), N_int, H_dmc_mat(i,j))
   enddo
 enddo

 do i=1,N_det
   call i_h_psi(psi_det(1,1,i), psi_det, psi_coef, N_int, N_det, &
        N_det, 1, H_dmc_mat(i,N_det+1) )
   H_dmc_mat(N_det+1,i) = h_dmc_row(i)
 enddo
 H_dmc_mat(mat_size,mat_size) = E_dmc - nuclear_repulsion

END_PROVIDER

BEGIN_PROVIDER [ double precision, S_dmc_mat, (mat_size, mat_size) ]
 implicit none
 BEGIN_DOC
! Overlap matrix extended with DMC data
 END_DOC
 integer :: i,j
 S_dmc_mat = 0.d0
 do i=1,mat_size
   S_dmc_mat(i,i) = 1.d0
 enddo

 do i=1,N_det
   S_dmc_mat(i,N_det+1) = psi_coef(i,1)
   S_dmc_mat(N_det+1,i) = S_dmc_row(i)
 enddo

END_PROVIDER



