# !!!
import numpy as np
from QR import QR_fact
# !!!
def powit_RSVD(X, new_r, nb_powit, nb_oversamp):
    # !!!
    G  = np.random.randn(X.shape[1], new_r+nb_oversamp)
    Q  = QR_fact( np.dot(X,G) )
    # !!!
    for _ in range(nb_powit):
        Q = QR_fact( np.dot(X.T,Q) )
        Q = QR_fact( np.dot(X,Q) )
        # !!!
    Y = np.dot(Q.T,X)
    # !!!
    U, S, VT = np.linalg.svd(Y, full_matrices=0)
    U = np.dot(Q,U)
    return U[:,:(new_r)], S[:(new_r)], VT[:(new_r),:]
    # !!!
# !!!