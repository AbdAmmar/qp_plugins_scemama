# !!!
import numpy as np
# !!!
def QR_fact(X):
    Q, R     = np.linalg.qr(X, mode="reduced")
    D        = np.diag( np.sign( np.diag(R) ) )
    Qunique  = np.dot(Q,D)
    #Runique = np.dot(D,R)
    return(Qunique)
# !!!