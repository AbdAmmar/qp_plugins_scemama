program print_ij_H_kl_det_v1

  implicit none

  BEGIN_DOC
  ! perturbative approach to build psi_postsvd
  ! without OMP
  END_DOC

  read_wf = .True.
  TOUCH read_wf

  PROVIDE N_int

  call run()
end


subroutine run

  implicit none

  integer(bit_kind)             :: det1(N_int,2), det2(N_int,2)
  integer                       :: degree
  double precision              :: h12

  det1(:,1) = psi_det_alpha_unique(:,1)
  det2(:,1) = psi_det_alpha_unique(:,1)
  det1(:,2) = psi_det_beta_unique(:,1)
  det2(:,2) = psi_det_beta_unique(:,1)
  call get_excitation_degree_spin(det1(1,1),det2(1,1),degree,N_int)
  call get_excitation_degree(det1,det2,degree,N_int)
  call i_H_j(det1, det2, N_int, h12)

  print *, ' ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~'
  print *, ' CI   matrix:', n_det_alpha_unique,'x',n_det_beta_unique
  print *, ' N det      :', N_det
  print *, ' ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~'


  ! get < d_i d_j | H | d_k d_l > in det basis
  call const_ij_Hdet_kl_sparse()

end




! _________________________________________________________________________________________________
!
subroutine const_ij_Hdet_kl_sparse()

  implicit none

  integer(bit_kind)             :: det1(N_int,2), det2(N_int,2)
  integer                       :: degree

  integer                       :: na, nb, i, j, k, l, ii
  double precision              :: h12

  double precision              :: t1, t2

  print *, ""
  print *, " start const_ij_Hdet_kl_sparse"
  call wall_time(t1)

  na = n_det_alpha_unique
  nb = n_det_beta_unique

  ii = 0
 !$OMP PARALLEL DEFAULT(NONE) PRIVATE(i,j,k,l,h12,det1,det2,degree)        &
 !$OMP          SHARED(na,nb,psi_det_alpha_unique,psi_det_beta_unique,N_int)
 !$OMP DO
  do l = 1, nb
    det2(:,2) = psi_det_beta_unique(:,l)
    do j = 1, nb
      det1(:,2) = psi_det_beta_unique(:,j)

      call get_excitation_degree_spin(det1(1,2),det2(1,2),degree,N_int)
      if(degree .gt. 2) cycle

      do k = 1, na
        det2(:,1) = psi_det_alpha_unique(:,k)
        do i = 1, na
          det1(:,1) = psi_det_alpha_unique(:,i)

          call get_excitation_degree(det1,det2,degree,N_int)
          if(degree .gt. 2) cycle

          call i_H_j(det1, det2, N_int, h12)
          if(dabs(h12) .le. (1d-9)) cycle

          write(7001, '(4(I8,2X),(F15.8))') i, j, k, l, h12

        enddo
      enddo
    enddo
  enddo
 !$OMP END DO
 !$OMP END PARALLEL

  call wall_time(t2)
  print *, " end const_ij_Hdet_kl_sparse after (min) ", (t2-t1)/60.
  print *, ""

  return
end subroutine const_ij_Hdet_kl_sparse
! _________________________________________________________________________________________________
! _________________________________________________________________________________________________

