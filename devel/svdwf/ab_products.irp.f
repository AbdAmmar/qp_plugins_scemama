program abproducts
  implicit none
  read_wf = .True.
  TOUCH read_wf
  call run
end

subroutine run
  implicit none
  call generate_all_alpha_beta_det_products
  call diagonalize_ci
  call save_wavefunction
end
