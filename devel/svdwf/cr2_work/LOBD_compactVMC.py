# !!!
import sys, os
QMCCHEM_PATH=os.environ["QMCCHEM_PATH"]
sys.path.insert(0,QMCCHEM_PATH+"/EZFIO/Python/")


from ezfio import ezfio
from math import sqrt
from datetime import datetime
import time
import numpy as np
import subprocess
from scipy.linalg import eig, eigh



# ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! 
def get_energy():
    buffer = subprocess.check_output(['qmcchem', 'result', '-e', 'e_loc', EZFIO_file],  encoding='UTF-8')
    if buffer.strip() != "":
        buffer = buffer.splitlines()[-1]
        _, energy, error = [float(x) for x in buffer.split()]
        return energy, error
    else:
      return None, None
# ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! 



# ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! 
def get_Ci_h_matrix_modifsvd():

    Ci_h_matrix_modifsvd = np.zeros( (n_modifsvd,n_modifsvd) )
    SErr_H               = np.zeros( (n_modifsvd,n_modifsvd) )

    beg_Ci_h_matrix_modifsvd = results.find('Ci_h_matrix_modifsvd : [ ') + len( 'Ci_h_matrix_modifsvd : [ ' )
    end_Ci_h_matrix_modifsvd = len(results)
    Ci_h_matrix_modifsvd_buf = results[beg_Ci_h_matrix_modifsvd:end_Ci_h_matrix_modifsvd]
    Ci_h_matrix_modifsvd_buf = Ci_h_matrix_modifsvd_buf.split( '\n' )

    for iline in range(1, n_modifsvd**2+1):

        line = Ci_h_matrix_modifsvd_buf[iline].split()
        indc = int( line[0] )
        errS = float( line[4] )
        if( indc != iline ):
            print(' Error in reading Ci_h_matrix_modifsvd')
            print(' indc  = {}'.format(indc) )
            print(' iline = {}'.format(iline) )
            break
        else:
          irow = indc %  n_modifsvd
          icol = indc // n_modifsvd
          if( irow!=0 ):
              Ci_h_matrix_modifsvd[irow-1][icol] = float( line[2] )
              SErr_H              [irow-1][icol] = float( line[4] )
          else:
              Ci_h_matrix_modifsvd[n_modifsvd-1][icol-1] = float( line[2] )
              SErr_H              [n_modifsvd-1][icol-1] = float( line[4] )
        # !!!
    # !!!

    return(Ci_h_matrix_modifsvd,SErr_H)
# ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! 



# ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! 
def get_Ci_overlap_matrix_modifsvd():

    Ci_overlap_matrix_modifsvd = np.zeros( (n_modifsvd,n_modifsvd) )
    SErr_O                     = np.zeros( (n_modifsvd,n_modifsvd) )

    beg_Ci_overlap_matrix_modifsvd = results.find('Ci_overlap_matrix_modifsvd : [ ') + len( 'Ci_overlap_matrix_modifsvd : [ ' )
    end_Ci_overlap_matrix_modifsvd = len(results)
    Ci_overlap_matrix_modifsvd_buf = results[beg_Ci_overlap_matrix_modifsvd:end_Ci_overlap_matrix_modifsvd]
    Ci_overlap_matrix_modifsvd_buf = Ci_overlap_matrix_modifsvd_buf.split( '\n' )

    for iline in range(1, n_modifsvd**2+1):

        line = Ci_overlap_matrix_modifsvd_buf[iline].split()
        indc = int( line[0] )
        errS = float( line[4] )
        if( indc != iline ):
            print(' Error in reading Ci_overlap_matrix_modifsvd')
            print(' indc  = {}'.format(indc))
            print(' iline = {}'.format(iline))
            break
        else:
          irow = indc %  n_modifsvd
          icol = indc // n_modifsvd
          if( irow!=0 ):
              Ci_overlap_matrix_modifsvd[irow-1][icol] = float( line[2] )
              SErr_O                    [irow-1][icol] = float( line[4] )
          else:
              Ci_overlap_matrix_modifsvd[n_modifsvd-1][icol-1] = float( line[2] )
              SErr_O                    [n_modifsvd-1][icol-1] = float( line[4] )

    return(Ci_overlap_matrix_modifsvd,SErr_O)
# ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! 




# ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! 
def check_symmetric(a, tol=1e-3):
    return np.all(np.abs(a-a.T) < tol)
# ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! 



# ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! 
def get_Esvd():

    Ci_h_matrix_modifsvd      , SErr_H = get_Ci_h_matrix_modifsvd()
    Ci_overlap_matrix_modifsvd, SErr_O = get_Ci_overlap_matrix_modifsvd()

    if( save_compactSVD_QMCmat ):
        with open('compactSVD_QMCmat.txt', 'a') as ff:
            for i in range(n_modifsvd):
                for j in range(n_modifsvd):
                    ff.write('{}  {}   {}   {}    {}  {} \n'.format(i,j
                                     , Ci_h_matrix_modifsvd[i,j]       , SErr_H[i,j]
                                     , Ci_overlap_matrix_modifsvd[i,j] , SErr_O[i,j] ))

    # symmetrise and diagonalise
    aa = Ci_h_matrix_modifsvd
    aa = 0.5*( aa + aa.T )
    bb = Ci_overlap_matrix_modifsvd
    eigvals_modifsvd, vr = eig(aa, bb, left=False, right=True, overwrite_a=True, overwrite_b=True,
            check_finite=True, homogeneous_eigvals=False)

    psi_tmp           = psi_svd_coefs[0,0:n_modifsvd]
    recouvre_modifsvd = np.abs(psi_tmp @ vr)
    ind_gssvd = np.argmax(recouvre_modifsvd)

    E_modifsvd = eigvals_modifsvd[ind_gssvd] + E_toadd

    return( E_modifsvd, vr[:,ind_gssvd] )
# ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! ~ ! 




if __name__ == '__main__':

    t0 = time.time()

    # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ #
    #EZFIO_file = "Cr2_compactSVD30"



    EZFIO_file = "Cr2_compactSVD30_it1"

    E_toadd = 64.8242130309350
    # ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ #

    ezfio.set_file(EZFIO_file)
    n_svd         = ezfio.get_spindeterminants_n_svd_coefs()
    n_modifsvd    = ezfio.get_spindeterminants_n_modifsvd_coefs()
    psi_svd_coefs = np.array(ezfio.get_spindeterminants_psi_svd_coefs())

    print(" ")
    print(" Today's date:", datetime.now() )
    print(" EZFIO file = {}".format(EZFIO_file))
    print(" n_svd      = {}".format(n_svd) )
    print(" n_modifsvd = {}\n".format(n_modifsvd) )

    t_read = time.time()
    print(' getting QMCCHEM results from {}'.format(EZFIO_file) )
    results =  subprocess.check_output(['qmcchem', 'result', EZFIO_file],  encoding='UTF-8')
    print(' getting results after {} minutes \n'.format( (time.time()-t_read)/60. ))

    if( len(results)<100):
        print(' result is empty \n')
        exit()

    Eloc, ErrEloc = get_energy()
    print(' Eloc = {} +/- {}'.format(Eloc, ErrEloc))
    print(' {} <= Eloc <= {}'.format(Eloc-ErrEloc,Eloc+ErrEloc))
    #print(' initial svd coeff = {}\n'.format(psi_svd_coefs))

    save_compactSVD_QMCmat = True

    print('')
    E_compactsvd, sigma_compactsvd = get_Esvd()
    print(' compact svd enegry = {}'.format(E_compactsvd) )
    sigma_compactsvd = sigma_compactsvd * np.sign(sigma_compactsvd[0])
    print(' compact svd coeff  = {}\n'.format(sigma_compactsvd))

    #print(' compact svd factor  = {}\n'.format(sigma_compactsvd[n_modifsvd-1]))

    # ___________________________________________________________________________
    # ___________________________________________________________________________
    # 
    #save_to_EZFIO = 'compactsvd_QMC'
    save_to_EZFIO = ''

    if( save_to_EZFIO == 'compactsvd_QMC' ):
        ezfio_sigma_svd = np.zeros((n_svd))

        for i in range(n_modifsvd-1):
            ezfio_sigma_svd[i] = sigma_compactsvd[i]
        for i in range(n_modifsvd-1, n_svd):
            ezfio_sigma_svd[i] = psi_svd_coefs[0,i] * sigma_compactsvd[n_modifsvd-1]

        #print(' new svd coeff  = {}\n'.format(ezfio_sigma_svd))

        ezfio.set_spindeterminants_psi_svd_coefs( ezfio_sigma_svd )
        direc_svdcoeff = EZFIO_file + '/spindeterminants/psi_svd_coefs.gz'
        print(' {} is modified'.format(direc_svdcoeff) )
    else:
        print(' no modifications has been done in {}'.format(EZFIO_file))
    # ___________________________________________________________________________
    # ___________________________________________________________________________

    print("end after {:.3f} minutes".format((time.time()-t0)/60.) )

    # !!!
# !!!

~                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
~                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
      
