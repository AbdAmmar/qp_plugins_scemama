program dmc_dress_multistate

  BEGIN_DOC
  ! Program that extracts the lowest states of the Hamiltonian dressed by the QMC
  ! dressing vector stored in :option:`dmc_dressing dmc_delta_h`
  !
 END_DOC

  implicit none

  read_wf = .True.
  touch read_wf
 
  call routine()
  call save_wavefunction_general(N_det, N_states, psi_det_sorted, size(psi_coef_sorted, 1), psi_coef_sorted)

end

! ---

subroutine routine

  implicit none
  integer :: k

  do k = 1, N_states
    print *, ' state:', k
    psi_coef(1:N_det,k) = ci_eigenvectors_nonsym_dressed(1:N_det,k)
    print *, 'E = ', ci_energy_nonsym_dressed(k) + nuclear_repulsion
  enddo

  SOFT_TOUCH psi_coef

end

! ---

