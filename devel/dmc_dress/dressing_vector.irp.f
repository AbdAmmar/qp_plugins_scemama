
! ---

 BEGIN_PROVIDER [ double precision, dressing_column_h, (N_det,N_states) ]
&BEGIN_PROVIDER [ double precision, dressing_column_s, (N_det,N_states) ]

  BEGIN_DOC
  ! \Delta_{state-specific}. \Psi
  ! Diagonal element is divided by 2 because Delta = D + D^t
  END_DOC

  implicit none
  integer :: i,ii,k,j, l
  double precision :: f, tmp
  double precision, allocatable :: delta(:)

  allocate(delta(N_det))
  delta(1:N_det) = dmc_delta_h(1:N_det,1)

  call dset_order(delta,psi_bilinear_matrix_order_reverse,N_det)

  dressing_column_h(:,:) = 0.d0
  dressing_column_s(:,:) = 0.d0

  l = dressed_column_idx(1)
  do j = 1, n_det
    if(j == l) cycle
    dressing_column_h(j,1)  = delta(j) 
    dressing_column_h(l,1) -= psi_coef(j,1) * delta(j) / psi_coef(l,1)
  enddo
  dressing_column_h(l,1) += delta(l) 
  dressing_column_h(l,1) *= 0.5d0

  deallocate(delta)

END_PROVIDER

! ---

BEGIN_PROVIDER [ double precision, dressing_delta, (N_det, N_states) ]

  BEGIN_DOC
  ! 
  ! dressing_delta is:
  ! [\delta_K]_I = < I | \tilde{H} - H | \Phi_K >
  !
  END_DOC

  implicit none
  integer                       :: i, j, k
  double precision, allocatable :: delta(:,:)

  dressing_delta(1:N_det,1:N_states) = 0.d0
  
  allocate(delta(N_det,N_states))

  do k = 1, N_states

    do j = 1, N_det
      delta(j,k) = dmc_delta_h(j,k)
    enddo

    call dset_order(delta(1:N_det,k), psi_bilinear_matrix_order_reverse, N_det)
    
    do j = 1, N_det
      dressing_delta(j,k) = delta(j,k) 
    enddo

  enddo

  deallocate(delta)

END_PROVIDER

! ---

BEGIN_PROVIDER [ double precision, dmc_delta_htc , (n_det) ]
  implicit none
  BEGIN_DOC
! Dressing matrix obtained from H_TC
  END_DOC

  logical                        :: has
  PROVIDE ezfio_filename
  if (mpi_master) then
    if (size(dmc_delta_htc) == 0) return

    call ezfio_has_dmc_dress_dmc_delta_htc(has)
    if (has) then
      write(6,'(A)') '.. >>>>> [ IO READ: dmc_delta_htc ] <<<<< ..'
      call ezfio_get_dmc_dress_dmc_delta_htc(dmc_delta_htc)
    else
      dmc_delta_htc(:) = 0.d0
    endif
  endif
  IRP_IF MPI_DEBUG
    print *,  irp_here, mpi_rank
    call MPI_BARRIER(MPI_COMM_WORLD, ierr)
  IRP_ENDIF
  IRP_IF MPI
    include 'mpif.h'
    integer :: ierr
    call MPI_BCAST( dmc_delta_htc, (n_det), MPI_DOUBLE_PRECISION, 0, MPI_COMM_WORLD, ierr)
    if (ierr /= MPI_SUCCESS) then
      stop 'Unable to read dmc_delta_htc with MPI'
    endif
  IRP_ENDIF

  call write_time(6)

END_PROVIDER
