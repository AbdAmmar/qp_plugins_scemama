BEGIN_PROVIDER [ double precision, r1_cc, (spin_occ_num,spin_vir_num) ]
 implicit none
 BEGIN_DOC
 ! Residues for t1 in non-canonical CCSD
 END_DOC

  integer                       :: i,j,m,n
  integer                       :: a,b,e,f

  r1_cc(:,:) = spin_fock_matrix_mo_ov(:,:)

  call dgemm('N','T',                                                &
      spin_occ_num, spin_vir_num, spin_vir_num,                      &
      1.d0, t1_cc, size(t1_cc,1),                                    &
      c_spin_fock_matrix_mo_vv, size(c_spin_fock_matrix_mo_vv,1),    &
      1.d0, r1_cc, size(r1_cc,1))
  
  call dgemm('T','N',                                                &
      spin_occ_num, spin_vir_num, spin_occ_num,                      &
      -1.d0, c_spin_fock_matrix_mo_oo, size(c_spin_fock_matrix_mo_oo,1),&
      t1_cc, size(t1_cc,1),                                          &
      1.d0, r1_cc, size(r1_cc,1))
  

  call dgemv('T', spin_occ_num*spin_vir_num,                         &
      spin_occ_num*spin_vir_num,                                     &
      1.d0, t2_cc2, spin_occ_num*spin_vir_num,                       &
      c_spin_fock_matrix_mo_ov, 1,                                   &
      1.d0, r1_cc, 1)
  
  double precision, external :: u_dot_v

  do f=1,spin_vir_num
    do a=1,spin_vir_num
      do i=1,spin_occ_num
        r1_cc(i,a) = r1_cc(i,a) - u_dot_v(t1_cc(1,f), OVOV(1,a,i,f),spin_occ_num)
      end do
    end do

    do e=1,spin_vir_num
      call dgemm('N','N', &
        spin_occ_num, spin_vir_num, spin_occ_num, &
        -0.5d0, t2_cc(1,1,e,f), spin_occ_num, &
        OVVV(1,1,e,f), spin_occ_num, &
        1.d0, r1_cc, spin_occ_num)
    enddo
  enddo

  call dgemm('T','N', &
     spin_occ_num, spin_vir_num, spin_occ_num*spin_occ_num*spin_vir_num, &
     -0.5d0, OOVO, spin_occ_num*spin_occ_num*spin_vir_num, &
     t2_cc, spin_occ_num*spin_occ_num*spin_vir_num, &
     1.d0, r1_cc, spin_occ_num)

! Final expression for t1 residue

  r1_cc(:,:) = delta_ov(:,:)*t1_cc(:,:) - r1_cc(:,:)

END_PROVIDER
