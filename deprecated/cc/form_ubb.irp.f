subroutine form_ubb(ubb)

! Form 2nd term in (T) correction

  implicit none

! Local variables

  integer                       :: i,j,k,l,m
  integer                       :: a,b,c,d,e

! Output variables

  double precision,intent(out)  :: ubb(spin_occ_num,spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num,spin_vir_num)

  ubb(:,:,:,:,:,:) = 0d0

  do c=1,spin_vir_num
    do b=1,spin_vir_num
      do a=1,spin_vir_num
        do k=1,spin_occ_num
          do j=1,spin_occ_num
            do i=1,spin_occ_num

              do e=1,spin_vir_num
                ubb(i,j,k,a,b,c) = ubb(i,j,k,a,b,c)          &
                                 + t2_cc(i,j,a,e)*VVVO(b,c,e,k) & 
                                 + t2_cc(i,j,b,e)*VVVO(c,a,e,k) & 
                                 + t2_cc(i,j,c,e)*VVVO(a,b,e,k) & 
                                 + t2_cc(k,i,a,e)*VVVO(b,c,e,j) & 
                                 + t2_cc(k,i,b,e)*VVVO(c,a,e,j) & 
                                 + t2_cc(k,i,c,e)*VVVO(a,b,e,j) & 
                                 + t2_cc(j,k,a,e)*VVVO(b,c,e,i) & 
                                 + t2_cc(j,k,b,e)*VVVO(c,a,e,i) & 
                                 + t2_cc(j,k,c,e)*VVVO(a,b,e,i)
              end do
 
              do m=1,spin_occ_num
                ubb(i,j,k,a,b,c) = ubb(i,j,k,a,b,c)          &
                                 + t2_cc(i,m,a,b)*VOOO(c,m,j,k) & 
                                 + t2_cc(i,m,b,c)*VOOO(a,m,j,k) & 
                                 + t2_cc(i,m,c,a)*VOOO(b,m,j,k) & 
                                 + t2_cc(j,m,a,b)*VOOO(c,m,k,i) & 
                                 + t2_cc(j,m,b,c)*VOOO(a,m,k,i) & 
                                 + t2_cc(j,m,c,a)*VOOO(b,m,k,i) & 
                                 + t2_cc(k,m,a,b)*VOOO(c,m,i,j) & 
                                 + t2_cc(k,m,b,c)*VOOO(a,m,i,j) & 
                                 + t2_cc(k,m,c,a)*VOOO(b,m,i,j)
              end do
              
            end do
          end do
        end do
      end do
    end do
  end do

end subroutine form_ubb
