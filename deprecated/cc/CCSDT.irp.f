subroutine CCSDT(EcCCT)

! Compute the (T) correction of the CCSD(T) energy

  implicit none

! Local variables

  double precision,allocatable  :: ub(:,:,:,:,:,:)
  double precision,allocatable  :: ubb(:,:,:,:,:,:)

! Output variables

  double precision,intent(out)  :: EcCCT

! Memory allocation

  allocate(ub(spin_occ_num,spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num,spin_vir_num),ubb(spin_occ_num,spin_occ_num,spin_occ_num,spin_vir_num,spin_vir_num,spin_vir_num))

! Form CCSD(T) quantities

  call form_ub(ub)

  call form_ubb(ubb)

  call form_T(ub,ubb,EcCCT)

end subroutine CCSDT
