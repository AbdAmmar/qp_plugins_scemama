===============
CHAMP Interface
===============

Interface from Quantum Package to
[CHAMP](https://www.utwente.nl/en/tnw/ccp/research/CHAMP.html).


For multi-state calculations, to extract state 2 use:

.. code-block:: bash

   QP_STATE=2 qp_run save_for_champ x.ezfio

(state 1 is the ground state).


